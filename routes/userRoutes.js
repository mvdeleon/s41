const express = require("express");
const router = express.Router();


const userControllers = require("../controllers/userControllers");
const auth = require("../auth");


router.post("/checkEmail",(req,res)=>{
	userControllers.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})


router.post("/register",(req,res)=>{
	userControllers.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

router.post("/login",(req,res)=>{
	userControllers.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
});

router.get("/details",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userControllers.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});


// Enroll

router.post("/enroll",auth.verify,(req,res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userControllers.enroll(data).then(resultFromController=>res.send(resultFromController))
})




module.exports = router;